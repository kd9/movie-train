<?php include "./inc/header.php"; ?>
</div>
<section class="container home">
  <h1 class="logo">Movie on Train</h1>
    <p class="centered">Welcome to our application. Please select a departure and arrival station, and we would suggest you a movie that you can enjoy.</p>
  <form method="post" action="reccommendation.php">
    <div class="select_container">
      <div class="select left">
        <label for="depart">Departure</label><br/>
        <select id="depart" name="depart">
          <option disabled selected>No stations found</option>
        </select>
      </div>
      <div class="select right">
        <label for="arrive">Arrival</label><br/>
        <select id="arrive" name="arrive">
          <option disabled selected>No stations found</option>
        </select>
      </div>
    </div>
    <input class="submit" type="submit">
  </form>
</section>
<?php include "./inc/footer.php"; ?>
