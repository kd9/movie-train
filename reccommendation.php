<?php
include "./inc/header.php";
include "./inc/ns.php";

$stops = get("stops", $_POST['depart'], $_POST['arrive']);
$duration = get("duration", $_POST['depart'], $_POST['arrive']);
$duration_seconds = get("duration_seconds", $_POST['depart'], $_POST['arrive']);

$genre = isset($_SESSION['genre']) ? $_SESSION['genre'] : 'Comedy';

?>
<input id="duration" type="hidden" value="<?=$duration_seconds?>">
<input id="genre_in" type="hidden" value="<?=$genre?>">
<a class="right" href="/">HOME</a>
</div>
<section class="container">
  <?php if(is_array($stops)): ?>
    <div class="route">
      <h2>Route <small>(<?= $duration ?>)</small></h2>
      <ul id="route">
        <?php if(is_array($stops)){
          foreach ($stops as $stop){
            echo "<li>$stop</li>";
          }
        } else {
          echo $stops;
        } ?>
      </ul>
    </div>

    <div class="suggestions">
      <h2>Movie Suggestions</h2>
      <h3><?= strtoupper($genre) ?></h3>
      <div id="movies">Sorry, No movies found in this genre or runtime</div>
    </div>

  <?php else: ?>
    <div>
      <h2>An error has occurred</h2>
      <h3><?= $stops ?></h3>
    </div>
  <?php endif;?>

</section>
<?php include "./inc/footer.php"; ?>
