$(document).ready(function(){
  if($('#depart').length){
    var url = "http://localhost:5820/movietrain/query";
    var query = "PREFIX mt: <http://example.org/movietrain/> select ?station ?label where {?station a mt:TrainStation . ?station rdfs:label ?label .}";
    $.ajax({
        headers : {
            Accept: 'application/sparql-results+json'
        },
        url: url,
        data: {
            query: query
        },
        success: function(data) {
            var results = data.results.bindings;
            var a = "", t1, t2;
            $.each (results, function(index, value){
              t1 = value['station']['value'];
              t1 = t1.substring(30);
              t2 = value['label']['value'];
              a += '<option value="' + t1 + '">' + t2 + "</option>"
            });
            $('#arrive').html(a);
            $('#depart').html(a);
        }
    });
  }

  if($('#route').length){
    var train_length = $('#duration').val();
    var min = train_length / 2;

    if($('#genre_in').val() == "Any_genre"){
      genre = "?g";
    }else{
      genre = "dbr:" + $('#genre_in').val();
    }

    var url = "http://localhost:5820/movietrain/query";
    var query = "PREFIX mt: <http://example.org/movietrain/> PREFIX dbr: <http://dbpedia.org/resource/> SELECT DISTINCT ?movie ?rt WHERE { ?movie a mt:Movie . ?movie mt:runtime ?rt . ?movie mt:hasGenre "+genre+" FILTER(?rt > "+min+")FILTER(?rt <= "+train_length+")}";
    console.log(query);
    $.ajax({
        headers : {
            Accept: 'application/sparql-results+json'
        },
        url: url,
        data: {
            query: query
        },
        success: function(data) {
            var results = data.results.bindings;
            var a = "", t1, t2, hours, minutes;
            var i = 0;
            $.each (results, function(index, value){
              t1 = value['movie']['value'];
              t1 = t1.replace('http://dbpedia.org/resource/','');
              t1 = t1.replace(/_/g,' ');
              t1 = "<a href=\"/movie.php\">" + t1 + "</a>";
              t2 = value['rt']['value'];
              hours = Math.floor(t2/60/60);
              minutes = Math.floor(t2/60);
              t2 = hours + "h" + minutes%60 + "m";

              if(i < 20){
                  a += t1 + " <small>(" + t2 + ")</small>" + "<br/><br/>";
                  i++;
              }
            });
            if(a.trim()){
              $('#movies').html(a);
            }
            console.log('-' + a + '-');
        }
    });
  }

  if($('#genre').length){
    var url = "http://localhost:5820/movietrain/query";
    var query = "PREFIX mt: <http://example.org/movietrain/> SELECT  ?genre WHERE {  ?genre a mt:Genre .}";
    $.ajax({
        headers : {
            Accept: 'application/sparql-results+json'
        },
        url: url,
        data: {
            query: query
        },
        success: function(data) {
            var results = data.results.bindings;
            var a = "", t1,t2;
            a += "<option value='Any_genre'>Any Genre</option>";
            $.each (results, function(index, value){
              t1 = value['genre']['value'];
              t1 = t1.substring(28);
              a += '<option value="' + t1 + '">' + t1 + "</option>"
            });
            $('#genre').html(a);
          }
        });
      }
});
