<?php
function get($request, $from, $to){
  // $from = "AC";
  // $to = "HTO";
  $url  = "http://lehlouisle@gmail.com:UEUfllontE6VgCeR7FxCwYCGwn2p8-8TNuu1_-4zzsmF_rqomoSLww";
  $url .= "@webservices.ns.nl/ns-api-treinplanner?";
  $url .= "fromStation=$from&toStation=$to&departure=true";

  if($from == $to){
    return "Arrival and Departure stations are the same.<br/><a href='./'>Go back</a>";
  }

  $response_xml_data = file_get_contents($url);
  if(!$response_xml_data){
      return;
  }

  $data = simplexml_load_string($response_xml_data);
  if($request == "stops"){
    $return = array();
    if(isset($data->ReisMogelijkheid{0}->ReisDeel)){
      foreach ($data->ReisMogelijkheid{0}->ReisDeel as $reisDeel) {
        foreach ($reisDeel as $reisStop) {
          $name = $reisStop->Naam->__toString();
          if(!in_array($name, $return, true) && !empty($name)){
            array_push($return, $name);
          }
        }
      }
      return $return;
    }else{
      return "No Travel Information";
    }
  }

  if($request == "duration"){
    $duration = print_r($data->ReisMogelijkheid{0}->GeplandeReisTijd->__toString(), true);
    $arr = explode(":", $duration, 2);
    $return = "";
    if($arr[0] != 0){
      $return .= $arr[0] . "h";
    }
    return $return . "$arr[1]m";
  }

  if($request == "duration_seconds"){
    $duration = print_r($data->ReisMogelijkheid{0}->GeplandeReisTijd->__toString(), true);
    $arr = explode(":", $duration, 2);
    $hours = 0;
    if($arr[0] != 0){
      $hours = $arr[0];
    }
    $minutes = $arr[1];
    return ($hours * 60 * 60) + ($minutes * 60);
  }
}
?>
