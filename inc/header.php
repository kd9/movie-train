<?php
session_start();
if(!isset($_SESSION['genre']))
  $_SESSION['genre'] = 'Comedy';
?>

<html>
<head>
  <title>Movies on Train</title>

  <meta name="viewport" content="width=device-width, initial-scale=1">

  <link type="text/css" rel="stylesheet" href="./inc/stylesheet.css">
  <link href="https://fonts.googleapis.com/css?family=Montserrat" rel="stylesheet">

  <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
  <script src="./inc/script.js"></script>
</head>
<body>
<?php include "./inc/genre.php" ?>
