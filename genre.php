<?php include "./inc/header.php"; ?>
<a style="float:right;" href="/">HOME</a>
</div>
<section class="container">
  <h1>Genre</h1>
  <form method="post">
    <select id="genre" name="genre">
       <option disabled selected>No genres found</option>
    </select>
    <br/>
    <input class="submit_genre" type="submit">
    <?php if(isset($_SESSION['genre'])):?>
      <h4>Your current genre is: <b><?=$_SESSION['genre']?></b></h4>
    <?php endif;?>
  </form>
</section>
<?php
include "./inc/footer.php"; ?>
